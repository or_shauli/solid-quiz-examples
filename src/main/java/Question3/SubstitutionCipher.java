package Question3;

import Question1.CipherDbHandler;

public abstract class SubstitutionCipher {

    protected int gapSize;
    protected final int numberOfRotations;
    LetterRotator letterRotator = new LetterRotator();

    public SubstitutionCipher(int numberOfRotations, int gapSize) {
        this.numberOfRotations = numberOfRotations;
        this.gapSize = gapSize;
    }


    public String encrypt(String textToEncrypt) {
        return generateCipherResult(textToEncrypt, this.numberOfRotations);
    }


    public String decrypt(String textToDecrypt) {
        return generateCipherResult(textToDecrypt, -this.numberOfRotations);
    }

    protected String generateCipherResult(String textToCipher, int numberOfRotations) {
        StringBuilder cipherResult = new StringBuilder();
        for (int i = 0; i < textToCipher.length(); i++) {
            char letter = generateCipherLetter(textToCipher.charAt(i), i%gapSize, numberOfRotations);
            cipherResult.append(letter);
        }
        return cipherResult.toString();
    }

    protected char generateCipherLetter(char letterToCipher, int replaceLetter, int numberOfRotations) {
        if(replaceLetter > 0)
            return letterToCipher;
        else
            return letterRotator.rotateLetter(letterToCipher, numberOfRotations);
    }

}
