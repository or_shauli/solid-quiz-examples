package Question1;

public interface CipherDbHandler {
    void updateCipher(String text);

    void loadCipher(String text);

    void deleteCipher(int textId);
}
